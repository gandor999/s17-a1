console.log("Hello");

let studentArray = [];

function addStudent(student){
	studentArray.push(student);
}

function countStudent(){
	console.log("There are a total of " + studentArray.length + " students");
}

function printStudents(){
	studentArray.sort();

	studentArray.forEach(function(student){
	console.log(student);
});
}


function findStudent(nameOfStudent){
	studentArray.sort();

	let count = 0;
	let nameAccumulate = [];
	let originalCase = "";
	

	studentArray.forEach(function(student){

	let temp = student;

	if(nameOfStudent.toLowerCase() == student.toLowerCase()){
		count += 1;
		nameAccumulate.push(student);

		originalCase = temp;
	}
});

	if(count == 1){
		console.log(originalCase + " is an enrollee")
	}

	if(count > 1){
		console.log(nameAccumulate.join(',') + " are enrollees");
	}

	if(count == 0){
		console.log(nameOfStudent + " is not an enrollee");
	}
}



function removeStudent(student){
	let temp;
	for(let i = 0; i < studentArray.length; ++i){
		if(studentArray[i].toLowerCase() == student.toLowerCase()){
			temp = studentArray[i];
			studentArray.splice(i, 1);
			console.log(temp + " has been removed form the list");
			break;
		}
	}
}
